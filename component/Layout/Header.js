import { constant } from "@/helper/constant";
import { Login } from "@component/Login";
import ContextHook from "@helper/ContextHook";

import { signOut, useSession } from "next-auth/react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useContext, useState } from "react";
import { Dropdown } from "react-bootstrap";

export default function Header() {
  const [showSettingMenu, setShowSettingMenu] = useState(false);
  const router = useRouter();

  const { data: session, status, update } = useSession();
  const { userDetails } = useContext(ContextHook.LoginDetails);
  console.log("------userDetails-------", userDetails);
  const clickLogout = async () => {
    await update({
      ...session,
      data: false,
    });
    signOut({ redirect: false });
    router.push("/sports");
  };

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-dark">
      <div className="container-fluid">
        <a className="navbar-brand" href="#">
          <Image
            className="head_logo"
            src="/img/R444.png"
            alt="me"
            width="100"
            height="40"
          />
        </a>
        <button
          className="navbar-toggler mobToggle"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon " />
        </button>
        <button type="submit" className="logInbtn moblogBtn my_bet_remo">
          Login
          <i className="fa-solid fa-right-to-bracket" />
        </button>
        <div className="main_expo_box">
          <div className="user-blnc">
            <span>
              {" "}
              Main <strong> 1,060.00</strong>
            </span>
            <span className="exp-topcount">
              Exposure
              <a className="text-white">
                <strong>
                  (<span className="exp-red">0.00</span>){" "}
                </strong>
              </a>
            </span>
          </div>
          <div>
            <a className="a-referesh" href="/" title="Setting">
              <img src="/img/test.gif" />
            </a>
          </div>
          <div className="bet-setting ms-2 mr-2">
            <a
              className="a-setting"
              title="Setting"
              data-bs-toggle="modal"
              data-bs-target="#betSetting_modal"
            >
              <img src="/img/test.gif" />
            </a>
          </div>
        </div>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          {status !== "loading" && (
            <>
              {session?.user?.token ? (
                <div className="d-flex ms-auto mb-2 mb-lg-0 userFrm">
                  <div className="user-blnc">
                    <span>
                      Main
                      <strong> {userDetails?.balance}</strong>
                    </span>
                    <span className="exp-topcount">
                      Exposure
                      <a className="text-white">
                        <strong>
                          (<span className="exp-red">0.00</span>)
                        </strong>
                      </a>
                    </span>
                  </div>
                  <div className bis_skin_checked={1}>
                    <span className="head-refresh">
                      <a id="menuRefresh" style={{ cursor: "pointer" }}>
                        Refresh
                      </a>
                    </span>
                    <a className="a-referesh" href="/" title="Setting">
                      <img src="../../../assets/img/transparent.gif" />
                    </a>
                  </div>
                  <Dropdown className="dropdown account-wrap-new ">
                    <Dropdown.Toggle
                      className="myaccnt "
                      type="button"
                      id="dropdown-my-account"
                      data-bs-toggle="dropdown"
                      aria-expanded="false"
                    >
                      My Account
                    </Dropdown.Toggle>
                    <Dropdown.Menu className="dropdown-menu dropdown-menu-end drop_menuBg ">
                      <div className="dropdown-header text-left">
                        <strong>{userDetails?.username}</strong>
                      </div>
                      {constant.settingMenuList.map((menu) => (
                        <li key={menu?.name}>
                          <Link className="dropdown-item" href={menu?.link}>
                            {menu?.name}
                          </Link>
                        </li>
                      ))}
                      <li>
                        <div className="dropdown-item drp-logout">
                          <button
                            className="btn btn-primary logout-btn"
                            onClick={() => clickLogout()}
                          >
                            LOGOUT
                            <i
                              aria-hidden="true"
                              className="fa fa-sign-out ml-2"
                            />
                          </button>
                        </div>
                      </li>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              ) : (
                <Login />
              )}
            </>
          )}
        </div>
      </div>
    </nav>
  );
}
