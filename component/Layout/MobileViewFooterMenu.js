import { constant } from "@/helper/constant";
import Link from "next/link";
import React from "react";

function MobileViewFooterMenu() {
  return (
    <nav className="mobile-footer-menu">
      <ul>
        {constant.menuList.map((item) => (
          <li
            className={item?.name === "Sports" ? "main-nav" : "ver-menu"}
            key={item?.name}
          >
            <Link href={item?.link}>
              <img src={`/img/${item?.icon}`} className="icon-home" />
              {item?.name}
            </Link>
          </li>
        ))}
      </ul>
    </nav>
  );
}

export default MobileViewFooterMenu;
