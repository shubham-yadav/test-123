import React from "react";
import useSWR from "swr";
import { constant } from "@/helper/constant";
import { useRouter } from "next/router";

export default function MobileViewMenu() {
  const { data: eventList } = useSWR("getAllSports");
  const { data: activeSportId } = useSWR("activeSports");
  const router = useRouter();
  return (
    <div className="mobile-menu">
      <a className="a-search" />
      <nav className="sportGame">
        <div>
          <div className="nav nav-tabs" id="nav-tab" role="tablist">
            {constant.sportsList
              .filter((ele) =>
                eventList?.data?.some((item) => item?.sport_id === ele.sport_id)
              )
              .map((item, ind) => (
                <button
                  key={item?.name}
                  className={
                    "nav-link" +
                    (+activeSportId === item?.sport_id ? " active" : "")
                  }
                  data-bs-toggle="tab"
                  type="button"
                  role="tab"
                  aria-selected={ind === 0}
                  onClick={() =>
                    router.push(`/sports/?id=${+item?.sport_id}&data=test`)
                  }
                >
                  <img src="/img/test.gif" className={`icon-${item?.class}`} />
                  {item?.name}
                </button>
              ))}
          </div>
        </div>
      </nav>
    </div>
  );
}
