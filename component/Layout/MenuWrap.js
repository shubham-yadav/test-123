import { constant } from "@/helper/constant";
import Link from "next/link";
import useSWR from "swr";
import React from "react";
import { Dropdown } from "react-bootstrap";

export default function MenuWrap() {
  const { data: eventList } = useSWR("getAllSports");
  const { data: activeSportId } = useSWR("activeSports");
  return (
    <nav className="menu-wrap">
      <div className="full-wrap">
        <ul className="tab-menu pull-left">
          {constant.menuList.map((item) => (
            <li className="ver-menu" key={item?.name}>
              <Link href={item?.link}>{item?.name}</Link>
            </li>
          ))}

          {constant.sportsList
            .filter((ele) =>
              eventList?.data?.some((item) => item?.sport_id === ele.sport_id)
            )
            .map((item) => (
              <li
                className={
                  "ver-menu " +
                  (item?.sport_id === +activeSportId ? "active" : "")
                }
                key={item?.name}
              >
                <Link href={"/sports/" + item?.sport_id}>{item?.name}</Link>
              </li>
            ))}
        </ul>
        <ul className="tab-menu pull-right">
          <Dropdown className="ver-menu settingstack" >
            <Dropdown.Toggle
            
              className="myaccnt"
              type="dropdown"
              id="dropdownMenuButton2"
              // data-bs-toggle="dropdown"
              // aria-expanded="false"
            >
              Setting
              <i className="fa fa-cog" />
            </Dropdown.Toggle>
            <Dropdown.Menu
              className="dropdown-menu dropdown-menu-end drop_menuBg stack_setting"
              aria-labelledby="dropdownMenuButton2"
            >
              <div className="side-content" id="coinList">
                <h3>Stake</h3>
                <dl
                  className="setting-block stake-setting"
                  id="editCustomizeStakeList"
                >
                  <dd>
                    <input
                      _ngcontent-xry-c0
                      name="stack_value"
                      type="text"
                      className="ng-untouched ng-pristine ng-valid"
                    />
                  </dd>
                  <dd>
                    <input
                      name="stack_value"
                      type="text"
                      className="ng-untouched ng-pristine ng-valid"
                    />
                  </dd>
                  <dd>
                    <input
                      name="stack_value"
                      type="text"
                      className="ng-untouched ng-pristine ng-valid"
                    />
                  </dd>
                  <dd>
                    <input
                      _ngcontent-xry-c0
                      name="stack_value"
                      type="text"
                      className="ng-untouched ng-pristine ng-valid"
                    />
                  </dd>
                  <dd>
                    <input
                      name="stack_value"
                      type="text"
                      className="ng-untouched ng-pristine ng-valid"
                    />
                  </dd>
                  <dd>
                    <input
                      name="stack_value"
                      type="text"
                      className="ng-untouched ng-pristine ng-valid"
                    />
                  </dd>
                  <dd>
                    <input
                      name="stack_value"
                      type="text"
                      className="ng-untouched ng-pristine ng-valid"
                    />
                  </dd>
                  <dd>
                    <input
                      name="stack_value"
                      type="text"
                      className="ng-untouched ng-pristine ng-valid"
                    />
                  </dd>
                  <dd className="col-stake_edit">
                    <button className="btn-send ui-link" href="/" id="save">
                      Save
                    </button>
                  </dd>
                </dl>
              </div>
            </Dropdown.Menu>
          </Dropdown>
        </ul>
      </div>
    </nav>
  );
}
