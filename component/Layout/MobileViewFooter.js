import React from "react";

export default function MobileViewFooter() {
  return (
    <div className="mobile-footer">
      <dl className="support-wrap-mobile">
        <dt>Support</dt>
        <dd>
          <dl>
            <dt>Calling</dt>
            <dd>
              <a href="tel:7699629860">7699629860</a>
            </dd>
          </dl>
          <dl>
            <dt>WhatsApp</dt>
            <dd>
              <a href="https://api.whatsapp.com/send?phone=918272905898">
                +918272905898
              </a>
            </dd>
          </dl>
        </dd>
      </dl>
      <div className="support-wrap" bis_skin_checked={1}>
        <dl className="support-mail">
          <a className="rules-btn-home">Privacy Policy</a>
          <a className="rules-btn-home arrow">Rules and Regulations</a>
          <a className="rules-btn-home arrow">KYC</a>
          <a className="rules-btn-home arrow">Terms and Conditions</a>
          <a className="rules-btn-home arrow">Responsible Gambling</a>
        </dl>
      </div>
    </div>
  );
}
