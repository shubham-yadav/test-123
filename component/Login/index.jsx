import { FloatingField } from "@component/Common/FloatingField";
import ContextHook from "@helper/ContextHook";
import errorSchema from "@helper/errorSchema";
import { utils } from "@helper/utils";
import { postData } from "@services/swr";
import { signIn } from "next-auth/react";
import React, { useContext, useEffect, useState } from "react";
import { Spinner } from "react-bootstrap";

export function Login(prpos) {
  const { formClass } = prpos;
  const [inputValue, setInputValue] = useState({
    username: "",
    password: "",
  });
  const [formError, setFormError] = useState({
    username: "",
    password: "",
  });
  const [loading, setLoading] = useState(false);
  const { setLoginModal } = useContext(ContextHook.LoginContext);

  const submitLogin = async (e) => {
    e.preventDefault();
    const validationResult = await utils.checkFormError(
      inputValue,
      errorSchema.loginSchema
    );
    if (validationResult === true) {
      setLoading(true);
      const resp = await postData(["userLogin", inputValue]);
      if (resp?.data?.user_id) {
        await signIn("credentials", {
          redirect: false,
          email: inputValue?.username,
          data: JSON.stringify(resp?.data),
          token: resp?.data?.token,
          isAuthenticate: true,
          // callbackUrl: `/${window.location.origin}/sports`,
        });
        setFormError({});
      } else {
        setFormError({ resError: resp?.error });
      }
      setLoginModal(false);
      setLoading(false);
      setInputValue({});
    } else {
      setFormError(validationResult);
    }
  };

  const onInputChange = async (name, value) => {
    const stateObj = { ...inputValue, [name]: value };
    setInputValue(stateObj);
    if (utils.isObjectValueEmpty(formError)) {
      const validationResult = await utils.checkFormError(
        stateObj,
        errorSchema.loginSchema
      );
      if (validationResult === true) {
        setFormError({});
      } else {
        setFormError(validationResult);
      }
    }
  };

  return (
    <form
      className={formClass ? formClass : "d-flex ms-auto mb-2 mb-lg-0 userFrm"}
    >
      <>
        <FloatingField
          controlId="floatingInput"
          type="text"
          name="username"
          placeholder="Username"
          label="Username"
          className="form-control"
          focus={!!formError?.username}
          error={formError?.username || formError?.resError}
          value={inputValue.username}
          onChange={({ target: { name, value } }) => onInputChange(name, value)}
        />
        <FloatingField
          controlId="floatingInput"
          label="Password"
          labelClass=""
          type="password"
          placeholder="Password"
          name="password"
          focus={!!formError?.password}
          error={formError?.password}
          value={inputValue.password}
          onChange={({ target: { name, value } }) => onInputChange(name, value)}
        />
        <button
          type="button"
          className="logInbtn"
          onClick={(e) => submitLogin(e)}
          disabled={loading}
        >
          Login
          {loading ? (
            <Spinner style={{ width: "1rem", height: "1rem" }} />
          ) : (
            <i className="fa-solid fa-right-to-bracket" />
          )}
        </button>
      </>
    </form>
  );
}
