import { constant } from "@/helper/constant";
import { postData } from "@/services/swr";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import useSWR, { mutate } from "swr";

export function InplayTable() {
  const [filter, setFilter] = useState(1);
  const { data: inplayEventList, isLoading } = useSWR(
    ["inplayEvents", { filter }],
    postData
  );

  return (
    <div className="game-highlight-wrap">
      <ul className="nav nav-tabs my-3" id="myTab" role="tablist">
        {constant.playMode?.mapWithKey((item, key, ind) => (
          <li className="nav-item" role="presentation" key={key}>
            <button
              className={"nav-link " + (item?.mode === filter ? "active" : "")}
              data-bs-toggle="tab"
              type="button"
              role="tab"
              aria-selected="true"
              onClick={() => setFilter(item?.mode)}
            >
              {item?.type}
            </button>
          </li>
        ))}
      </ul>
      {inplayEventList?.data &&
        Object.keys(inplayEventList?.data?.records)?.map((item) => (
          <React.Fragment>
            <p className="bg_color py-1 px-2" role="presentation">
              {item}
            </p>
            <div
              className="tab-content game-menu-tab"
              id="myTabContent"
              bis_skin_checked={1}
            >
              <div
                className="tab-pane fade show active"
                id="cricket"
                role="tabpanel"
                aria-labelledby="cricket-tab"
                bis_skin_checked={1}
              >
                <table className="table table-hover">
                  <thead>
                    <tr>
                      <th className="view-mobile" colSpan={2} />
                      <th className="text-center">1</th>
                      <th className="text-center">X</th>
                      <th className="text-center">2</th>
                      <th className="text-center" />
                    </tr>
                  </thead>
                  <tbody className="_tbody">
                    {inplayEventList?.data?.records[item]?.mapWithKey(
                      (list, key) => (
                        <tr kry={key}>
                          <td>
                            <a href="index2.html" className="match-name">
                              <span>{list?.event_name}</span>
                            </a>
                            {/* <span className="in_play">In-Play</span> */}
                            <p className="match-time">{list?.open_date_ist}</p>
                          </td>
                          <td />
                          {/* <td>
            <span className="game-fancy mr-1">F</span>
            <span className="game-sportbook">S</span>
          </td> */}
                          <td className="count">
                            <span className="back">1.53</span>
                            <span className="lay">1.6</span>
                          </td>
                          <td className="count">
                            <span className="back">0.0</span>
                            <span className="lay">0.0</span>
                          </td>
                          <td className="count">
                            <span className="back">0</span>
                            <span className="lay">0</span>
                          </td>
                          <td className="pl-0">
                            <a
                              className="add-pin"
                              data-bs-toggle="modal"
                              data-bs-target="#exampleModal"
                            />
                          </td>
                        </tr>
                      )
                    )}
                  </tbody>
                </table>
              </div>
            </div>
          </React.Fragment>
        ))}
    </div>
  );
}
