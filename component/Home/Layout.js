import React from "react";
import { HomeComponent } from "..";

export function Layout({ children }) {
  return (
    <>
      <section className="hiLight">
        <div className="container-fluid" bis_skin_checked={1}>
          <div className="row" bis_skin_checked={1}>
            <HomeComponent.SportsList />
            {children}
            <HomeComponent.BetList />
          </div>
        </div>
      </section>
    </>
  );
}
