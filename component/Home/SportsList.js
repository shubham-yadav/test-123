import Link from "next/link";
import React from "react";
import { fetchData } from "@/services/swr";
import { useEffect, useState } from "react";
import useSWR, { mutate } from "swr";

export function SportsList() {
  const { data: eventList } = useSWR("getAllSportEvents");
  const { data: activeSportId } = useSWR("activeSports");
  const { data: sports } = useSWR("getAllSports", fetchData);
  const [activeLeague, setActiveLeague] = useState("");
  const [sportsEvent, setSportEvent] = useState("");

  useEffect(() => {
    if (activeSportId) {
      getLeagueEvents();
      setSportEvent("");
    }
  }, [activeSportId]);

  const getLeagueEvents = () => {
    mutate(
      "getAllSportEvents",
      fetchData("getAllSportEvents", { id: activeSportId })
    );
  };

  useEffect(() => {
    if (eventList?.data) {
      const leagueEvent = eventList?.data.reduce((itemArray, item) => {
        const eventObj = {
          event_id: item?.event_id,
          event_name: item?.event_name,
          league_id: item?.league_id,
        };
        if (itemArray?.length > 0) {
          const availInd = itemArray.findIndex(
            (e) => e?.league_id === item?.league_id
          );
          if (availInd >= 0) {
            itemArray[availInd].events.push(eventObj);
            itemArray.splice(availInd, 1, itemArray[availInd]);
          } else {
            itemArray.push({
              ...item,
              events: [eventObj],
            });
          }
        } else {
          itemArray.push({
            ...item,
            events: [eventObj],
          });
        }
        return itemArray;
      }, []);
      setSportEvent(leagueEvent);
    } else {
      setSportEvent([]);
    }
  }, [eventList]);

  return (
    <div className="art_colleft padRemoright col_fixed" bis_skin_checked={1}>
      <nav className="sidebar-nav ps ps--active-y">
        <div className="top-head-sport" bis_skin_checked={1}>
          <a className="path-back">
            <i className="fa-solid fa-ellipsis-vertical" />
          </a>
          <ul className="text-right mr-2">
            <li>
              <Link
                className={activeSportId !== "" ? "pointer" : ""}
                href={"/sports"}
                onClick={() =>
                  mutate("activeSports", () => {
                    return "";
                  })
                }
              >
                Sports
              </Link>
            </li>
          </ul>
        </div>
        {!activeSportId ? (
          <ul className="nav_gate">
            <div bis_skin_checked={1}>
              {sports?.data?.mapWithKey((sport, key) => (
                <li className="nav-item nav-dropdown" key={key}>
                  <Link
                    className="nav-link nav-dropdown-toggle"
                    type="button"
                    href={"/sports/" + sport?.sport_id}
                  >
                    {sport?.sports_name}
                  </Link>
                </li>
              ))}
            </div>
          </ul>
        ) : (
          <ul className="nav_gate accordion" id="accordionExample">
            {sportsEvent
              ? sportsEvent?.mapWithKey((item, key) => (
                  <React.Fragment key={key}>
                    <li
                      className="nav-item nav-dropdown"
                      id="headingOne"
                    >
                      <p
                        className="nav-link nav-dropdown-toggle"
                        type="button"
                        onClick={() => setActiveLeague(item?.league_id)}
                      >
                        {item?.league_name}
                      </p>
                    </li>
                    <div
                      id="collapseOne"
                      className={
                        (item?.league_id === activeLeague ? " " : "collapse ") +
                        "accordion-collapse "
                      }
                    >
                      <div className="accordion-body">
                        {item?.events?.length > 0 &&
                          item?.events.map((match) => (
                            <Link
                              className="nav-link"
                              replace
                              prefetch
                              href={`/Inplay/${match.event_id}`}
                              key={match.event_id}
                            >
                              <i className="fa-solid fa-arrow-right" />{" "}
                              {match?.event_name}
                            </Link>
                          ))}
                      </div>
                    </div>
                  </React.Fragment>
                ))
              : [1, 2].map((ind) => (
                  <li className="nav-item nav-dropdown skeleton" key={ind}></li>
                ))}
          </ul>
        )}
      </nav>
    </div>
  );
}
