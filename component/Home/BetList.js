import React from "react";

export function BetList() {
  return (
    <div className="aside  padRemoleft col_fixed fl-right">
      <div className="card open-bet mb-0 b-none">
        <div className="card-header card-accrdn openbetlist">
          <strong>Open Bets</strong>
        </div>
        <div className="tab-content" />
      </div>
    </div>
  );
}
