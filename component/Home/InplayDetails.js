import { useState } from "react";

export function InplayDetails() {
  const [betType, setBetType] = useState("Fancy");
  return (
    <div className="maincol ">
      <div>
        <div className=" ">
          <div>
            <div>
              <div className=" match-list">
                <div className="card mb-0 matchodd">
                  <div className="card-matchodds">
                    <strong className="match-odds">
                      Bookmaker (0% Commission Fast Bet Confirm )
                      <span className="marketinfo ml-2" />
                    </strong>
                    <marquee
                      className="pull-left hidden-xs"
                      style={{
                        width: "50%",
                        fontWeight: "bold",
                        lineHeight: "30px",
                      }}
                    >
                      {" "}
                    </marquee>
                    <span className="matched-count  pull-right">
                      Matched <strong>€ 16M</strong>
                    </span>
                    <marquee
                      _ngcontent-gwy-c3
                      className="pull-right show-xs"
                      style={{
                        width: "100%",
                        fontWeight: "bold",
                        lineHeight: "20px",
                      }}
                    >
                      {" "}
                    </marquee>
                  </div>
                  <div className="card-body p-0">
                    <div className="table-responsive">
                      <table
                        className="table table-hover betfair-tbl tbl-bets p-rltv"
                        style={{ marginBottom: 0, background: "#b3d8f7" }}
                      >
                        <thead className="match_thead">
                          <tr>
                            <th className />
                            <th
                              colSpan={3}
                              style={{
                                textAlign: "right",
                                paddingRight: "20px",
                              }}
                            >
                              Back
                            </th>
                            <th
                              colSpan={3}
                              style={{ textAlign: "left", paddingLeft: "20px" }}
                            >
                              Lay
                            </th>
                          </tr>
                        </thead>
                        <tbody className=" ">
                          <tr className="odd-row">
                            <td className="team-name team-width">
                              <span className="title">Kabul Eagles</span>
                              <span className="minus-book">
                                <i className="fa fa-arrow-right" /> 0.00
                              </span>
                            </td>
                            <td
                              colSpan={3}
                              style={{ padding: 0, position: "relative" }}
                            >
                              <div
                                className="suspend-bookmaker"
                                bis_skin_checked={1}
                              >
                                <span className="stats-text">Suspended</span>
                              </div>
                              <dl className="back-gradient">
                                <dd
                                  style={{ cursor: "pointer" }}
                                  className="count 52285324537 back_3"
                                >
                                  <a className="not-allowed">
                                    {" "}
                                    0<span>600K</span>
                                  </a>
                                </dd>
                                <dd
                                  style={{ cursor: "pointer" }}
                                  className="count 52285324537 back_2"
                                >
                                  <a className="not-allowed">
                                    {" "}
                                    0<span>400K</span>
                                  </a>
                                </dd>
                                <dd
                                  className="count 52285324537 back_1"
                                  style={{ cursor: "pointer" }}
                                >
                                  <a className="not-allowed">
                                    {" "}
                                    0<span>200K</span>
                                  </a>
                                </dd>
                              </dl>
                            </td>
                            <td colSpan={3} style={{ padding: 0 }}>
                              <dl className="lay-gradient">
                                <dd
                                  style={{ cursor: "pointer" }}
                                  className="count 52285324537 lay_1"
                                >
                                  <a className="not-allowed">
                                    {" "}
                                    0<span>200K</span>
                                  </a>
                                </dd>
                                <dd
                                  style={{ cursor: "pointer" }}
                                  className="count 52285324537 lay_2"
                                >
                                  <a className="not-allowed">
                                    {" "}
                                    0<span>400K</span>
                                  </a>
                                </dd>
                                <dd
                                  className="count 52285324537 lay_3"
                                  id="back_1"
                                  style={{ cursor: "pointer" }}
                                >
                                  <a className="not-allowed">
                                    {" "}
                                    0<span>600K</span>
                                  </a>
                                </dd>
                              </dl>
                            </td>
                          </tr>
                          <tr className="odd-row">
                            <td className="team-name team-width">
                              <span className="title">Boost Defenders</span>
                              <span className="minus-book">
                                <i className="fa fa-arrow-right" /> 0.00
                              </span>
                            </td>
                            <td
                              colSpan={3}
                              style={{ padding: 0, position: "relative" }}
                            >
                              {/* <div  class="suspend-bookmaker" bis_skin_checked="1">
                                <span  class="stats-text">Suspended</span>
                              </div> */}
                              <dl className="back-gradient">
                                <dd
                                  style={{ cursor: "pointer" }}
                                  className="count 52285324537 back_3"
                                >
                                  <a className="not-allowed">
                                    {" "}
                                    1.04
                                    <span>600K</span>
                                  </a>
                                </dd>
                                <dd
                                  style={{ cursor: "pointer" }}
                                  className="count 52285324537 back_2"
                                >
                                  <a className="not-allowed">
                                    {" "}
                                    1.07
                                    <span>400K</span>
                                  </a>
                                </dd>
                                <dd
                                  className="count 52285324537 back_1"
                                  style={{ cursor: "pointer" }}
                                >
                                  <a
                                    className="not-allowed"
                                    data-bs-toggle="modal"
                                    data-bs-target="#exampleModal_new"
                                  >
                                    {" "}
                                    1.1
                                    <span>200K</span>
                                  </a>
                                </dd>
                              </dl>
                            </td>
                            <td colSpan={3} style={{ padding: 0 }}>
                              <dl className="lay-gradient">
                                <dd
                                  style={{ cursor: "pointer" }}
                                  className="count 52285324537 lay_1"
                                >
                                  <a
                                    className="not-allowed"
                                    data-bs-toggle="modal"
                                    data-bs-target="#exampleModal_new2"
                                  >
                                    1.13
                                    <span>200K</span>
                                  </a>
                                </dd>
                                <dd
                                  style={{ cursor: "pointer" }}
                                  className="count 52285324537 lay_2"
                                >
                                  <a className="not-allowed">
                                    {" "}
                                    1.16
                                    <span>400K</span>
                                  </a>
                                </dd>
                                <dd
                                  className="count 52285324537 lay_3"
                                  id="back_1"
                                  style={{ cursor: "pointer" }}
                                >
                                  <a className="not-allowed">
                                    {" "}
                                    1.19
                                    <span>600K</span>
                                  </a>
                                </dd>
                              </dl>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <table className="bit-add_Table">
                        <tbody>
                          <tr
                            className="fancy-quick-tr slip-back "
                            style={{ display: "table-row" }}
                          >
                            <td
                              className="modal bitBrs1"
                              id="exampleModal_new"
                              data-bs-backdrop="false"
                              data-bs-keyboard="false"
                              tabIndex={-1}
                              aria-labelledby="exampleModalLabel"
                              aria-hidden="true"
                            >
                              <table className="bit-add_Table">
                                <tbody className="modal-dialog">
                                  <tr className="modal-content">
                                    <td className="modal-body">
                                      <dl
                                        className="quick_bet-wrap"
                                        id="classWrap"
                                      >
                                        <dt id="fancyBetHeader">
                                          <span
                                            className="bet-check"
                                            id="fancyBetAcceptCheck"
                                          />
                                        </dt>
                                        <dd className="col-btn">
                                          <a
                                            className="btn"
                                            id="cancel"
                                            style={{ cursor: "pointer" }}
                                            data-bs-dismiss="modal"
                                            aria-label="Close"
                                          >
                                            Cancel
                                          </a>
                                        </dd>
                                        {/* <dd  class="col-stake betfairodds">
                                        <a class="icon-minus" id="stakeDown"></a>
                                        <input type="number" class="  ng-untouched ng-pristine ng-valid">
                                        <a class="icon-plus" id="stakeUp"></a>
                                      </dd> */}
                                        <dd className="col-odd" id="oddsHeader">
                                          <ul className="quick-bet-confirm">
                                            <li id="runs">44</li>
                                            <li
                                              className="quick-bet-confirm-title"
                                              id="odds"
                                            >
                                              100
                                            </li>
                                          </ul>
                                        </dd>
                                        <dd className="col-stake">
                                          <a
                                            className="icon-minus show-xs"
                                            id="stakeDown"
                                          />
                                          <input
                                            type="number"
                                            className=" oddinput ng-valid ng-dirty ng-touched"
                                          />
                                          <a
                                            className="icon-plus show-xs"
                                            id="stakeUp"
                                          />
                                        </dd>
                                        <dd className="col-send">
                                          <button
                                            className="btn-send disable"
                                            id="placeBet"
                                            style={{ cursor: "pointer" }}
                                          >
                                            Place Bets
                                          </button>
                                        </dd>
                                        <dd
                                          className="col-stake_list"
                                          id="stakePopupList"
                                          style={{ display: "block" }}
                                        >
                                          <ul>
                                            <li>
                                              <a
                                                className="btn"
                                                style={{ cursor: "pointer" }}
                                              >
                                                1000
                                              </a>
                                            </li>
                                            <li>
                                              <a
                                                className="btn"
                                                style={{ cursor: "pointer" }}
                                              >
                                                5000
                                              </a>
                                            </li>
                                            <li>
                                              <a
                                                className="btn"
                                                style={{ cursor: "pointer" }}
                                              >
                                                10000
                                              </a>
                                            </li>
                                            <li>
                                              <a
                                                className="btn"
                                                style={{ cursor: "pointer" }}
                                              >
                                                25000
                                              </a>
                                            </li>
                                            <li>
                                              <a
                                                className="btn"
                                                style={{ cursor: "pointer" }}
                                              >
                                                50000
                                              </a>
                                            </li>
                                            <li>
                                              <a
                                                className="btn"
                                                style={{ cursor: "pointer" }}
                                              >
                                                100000
                                              </a>
                                            </li>
                                            <li>
                                              <a
                                                className="btn"
                                                style={{ cursor: "pointer" }}
                                              >
                                                200000
                                              </a>
                                            </li>
                                            <li>
                                              <a
                                                className="btn"
                                                style={{ cursor: "pointer" }}
                                              >
                                                500000
                                              </a>
                                            </li>
                                          </ul>
                                        </dd>
                                      </dl>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <table className="bit-add_Table">
                        <tbody>
                          <tr
                            className="fancy-quick-tr slip-lay  "
                            style={{ display: "table-row" }}
                          >
                            <td
                              className="modal bitBrs2"
                              id="exampleModal_new2"
                              data-bs-backdrop="false"
                              data-bs-keyboard="false"
                              tabIndex={-1}
                              aria-labelledby="exampleModalLabel2"
                              aria-hidden="true"
                            >
                              <table className="bit-add_Table">
                                <tbody className="modal-dialog">
                                  <tr className="modal-content">
                                    <td className="modal-body">
                                      <dl
                                        className="quick_bet-wrap"
                                        id="classWrap2"
                                      >
                                        <dt id="fancyBetHeader">
                                          <span
                                            className="bet-check"
                                            id="fancyBetAcceptCheck"
                                          />
                                        </dt>
                                        <dd className="col-btn">
                                          <a
                                            className="btn"
                                            id="cancel"
                                            style={{ cursor: "pointer" }}
                                            data-bs-dismiss="modal"
                                            aria-label="Close"
                                          >
                                            Cancel
                                          </a>
                                        </dd>
                                        {/* <dd  class="col-stake betfairodds">
                                        <a class="icon-minus" id="stakeDown"></a>
                                        <input type="number" class="  ng-untouched ng-pristine ng-valid">
                                        <a class="icon-plus" id="stakeUp"></a>
                                      </dd> */}
                                        <dd className="col-odd" id="oddsHeader">
                                          <ul className="quick-bet-confirm">
                                            <li id="runs">44</li>
                                            <li
                                              className="quick-bet-confirm-title"
                                              id="odds"
                                            >
                                              100
                                            </li>
                                          </ul>
                                        </dd>
                                        <dd className="col-stake">
                                          <a
                                            className="icon-minus show-xs"
                                            id="stakeDown"
                                          />
                                          <input
                                            type="number"
                                            className=" oddinput ng-valid ng-dirty ng-touched"
                                          />
                                          <a
                                            className="icon-plus show-xs"
                                            id="stakeUp"
                                          />
                                        </dd>
                                        <dd className="col-send">
                                          <button
                                            className="btn-send disable"
                                            id="placeBet"
                                            style={{ cursor: "pointer" }}
                                          >
                                            Place Bets
                                          </button>
                                        </dd>
                                        <dd
                                          className="col-stake_list"
                                          id="stakePopupList"
                                          style={{ display: "block" }}
                                        >
                                          <ul>
                                            <li>
                                              <a
                                                className="btn"
                                                style={{ cursor: "pointer" }}
                                              >
                                                1000
                                              </a>
                                            </li>
                                            <li>
                                              <a
                                                className="btn"
                                                style={{ cursor: "pointer" }}
                                              >
                                                5000
                                              </a>
                                            </li>
                                            <li>
                                              <a
                                                className="btn"
                                                style={{ cursor: "pointer" }}
                                              >
                                                10000
                                              </a>
                                            </li>
                                            <li>
                                              <a
                                                className="btn"
                                                style={{ cursor: "pointer" }}
                                              >
                                                25000
                                              </a>
                                            </li>
                                            <li>
                                              <a
                                                className="btn"
                                                style={{ cursor: "pointer" }}
                                              >
                                                50000
                                              </a>
                                            </li>
                                            <li>
                                              <a
                                                className="btn"
                                                style={{ cursor: "pointer" }}
                                              >
                                                100000
                                              </a>
                                            </li>
                                            <li>
                                              <a
                                                className="btn"
                                                style={{ cursor: "pointer" }}
                                              >
                                                200000
                                              </a>
                                            </li>
                                            <li>
                                              <a
                                                className="btn"
                                                style={{ cursor: "pointer" }}
                                              >
                                                500000
                                              </a>
                                            </li>
                                          </ul>
                                        </dd>
                                      </dl>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div
                  className="card"
                  style={{
                    border: "none",
                    marginTop: "30px",
                    padding: 0,
                    background: "none",
                    height: "5px",
                  }}
                >
                  <div className="p-rltv">
                    <div className="fancy-head">
                      <div
                        className="nav nav-tabs"
                        id="nav-tab_d"
                        role="tablist"
                        style={{ borderBottom: "0px solid #dee2e6" }}
                      >
                        <h4
                          onClick={() => setBetType("Fancy")}
                          className={
                            (betType === "Fancy"
                              ? " in-play-sportbook"
                              : "in-play") + " nav-link "
                          }
                          id="nav-home-tab_d"
                          data-bs-toggle="tab"
                          data-bs-target="#fancyBet_d"
                          type="button"
                          role="tab"
                          aria-controls="nav-home"
                          aria-selected="true"
                        >
                          <span id="headerName">Fancy Bet</span>
                          <a className="btn-fancybet_rules" />
                        </h4>
                        <h4
                          onClick={() => setBetType("Sportsbook")}
                          className={
                            (betType === "Sportsbook"
                              ? " in-play-sportbook "
                              : "") + "nav-link "
                          }
                          id="sportsBook-tab_d"
                          data-bs-toggle="tab"
                          data-bs-target="#sportsBook_d"
                          type="button"
                          role="tab"
                          aria-controls="nav-profile"
                          aria-selected="false"
                        >
                          <span>Sportsbook</span>
                          <a className="btn-sportsbook_rules" />
                        </h4>
                      </div>
                    </div>
                    <div
                      className="tab-content"
                      id="nav-tabContent"
                      style={{ height: "auto", border: 0 }}
                    >
                      <div
                        className="tab-pane  show active"
                        id="fancyBet_d"
                        role="tabpanel"
                        aria-labelledby="nav-home-tab_d"
                      >
                        <nav className="fancyBet-fabList">
                          <div
                            className="nav nav-tabs justify-content-center outer_navtablist"
                            id="nav-tab"
                            role="tablist"
                          >
                            <button
                              className="nav-link active"
                              id="nav-all-tab_d"
                              data-bs-toggle="tab"
                              data-bs-target="#nav-all_d"
                              type="button"
                              role="tab"
                              aria-controls="nav-home"
                              aria-selected="true"
                            >
                              All
                            </button>
                            <button
                              className="nav-link"
                              id="nav-fancy-tab_d"
                              data-bs-toggle="tab"
                              data-bs-target="#nav-fancy_d"
                              type="button"
                              role="tab"
                              aria-controls="nav-profile"
                              aria-selected="false"
                            >
                              Fancy
                            </button>
                            <button
                              className="nav-link"
                              id="nav-lineMarket-tab_d"
                              data-bs-toggle="tab"
                              data-bs-target="#nav-lineMarket_d"
                              type="button"
                              role="tab"
                              aria-controls="nav-contact"
                              aria-selected="false"
                            >
                              Line Market
                            </button>
                            <button
                              className="nav-link"
                              id="nav-ballByball-tab_d"
                              data-bs-toggle="tab"
                              data-bs-target="#nav-ballByball_d"
                              type="button"
                              role="tab"
                              aria-controls="nav-contact"
                              aria-selected="false"
                            >
                              Ball by Ball
                            </button>
                            <button
                              className="nav-link"
                              id="nav-meterMarket-tab_d"
                              data-bs-toggle="tab"
                              data-bs-target="#nav-meterMarket_d"
                              type="button"
                              role="tab"
                              aria-controls="nav-contact"
                              aria-selected="false"
                            >
                              Meter Market
                            </button>
                            <button
                              className="nav-link"
                              id="nav-khadoMarket-tab_d"
                              data-bs-toggle="tab"
                              data-bs-target="#nav-khadoMarke_dt"
                              type="button"
                              role="tab"
                              aria-controls="nav-contact"
                              aria-selected="false"
                            >
                              Khado Market
                            </button>
                          </div>
                        </nav>
                        <div
                          className="tab-content"
                          id="nav-tabContent"
                          style={{ height: "auto" }}
                        >
                          <div
                            className="tab-pane  show active"
                            id="nav-all_d"
                            role="tabpanel"
                            aria-labelledby="nav-all-tab_d"
                          >
                            <div className="card  fancy-card">
                              <div className="card-header card-accrdn card-fancybet">
                                <div className="card-body p-0">
                                  <div className="table-responsive">
                                    <table className="table table-hover tbl-bets fancy-bet p-rltv">
                                      <thead>
                                        <tr>
                                          <th className="minwidth" colSpan={2}>
                                            <marquee
                                              className="pull-right fancy-marquee"
                                              direction="scroll"
                                            >
                                              <strong />
                                            </marquee>
                                          </th>
                                          <th className="lay-1">No</th>
                                          <th className="back-1">Yes</th>
                                          <th className="hidden-xs">Min/Max</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr className="odd-row">
                                          <td className="team-name valign-middle">
                                            <a className="title">
                                              WI 6 Over Runs ADV{" "}
                                            </a>
                                            <span className="minus-book">
                                              <i className="fa fa-arrow-right" />
                                              0.00
                                            </span>
                                          </td>
                                          <td
                                            className="valign-middle mbl-book"
                                            style={{ position: "relative" }}
                                          >
                                            <button className="book-btn btn btn-primary ">
                                              Book
                                            </button>
                                          </td>
                                          <td
                                            id="fancy1012022730191742154"
                                            className="lay-1 count"
                                          >
                                            <a
                                              data-bs-toggle="modal"
                                              data-bs-target="#exampleModal_new3"
                                            >
                                              44
                                              <span>100</span>
                                            </a>
                                          </td>
                                          <td className="back-1 count">
                                            <a
                                              data-bs-toggle="modal"
                                              data-bs-target="#exampleModal_new4"
                                            >
                                              46
                                              <span>100</span>
                                            </a>
                                          </td>
                                          <td className="min-max hidden-xs">
                                            100-50000
                                          </td>
                                        </tr>
                                        <tr>
                                          <td colSpan={7} className="padd_remo">
                                            <table className="bit-add_Table">
                                              <tbody>
                                                <tr
                                                  className="fancy-quick-tr slip-lay "
                                                  style={{
                                                    display: "table-row",
                                                  }}
                                                >
                                                  <td
                                                    className="modal bitBrs2"
                                                    id="exampleModal_new3"
                                                    data-bs-backdrop="false"
                                                    data-bs-keyboard="false"
                                                    tabIndex={-1}
                                                    aria-labelledby="exampleModalLabel3"
                                                    aria-hidden="false"
                                                  >
                                                    <table className="bit-add_Table">
                                                      <tbody className="modal-dialog">
                                                        <tr className="modal-content">
                                                          <td className="modal-body">
                                                            <dl
                                                              className="quick_bet-wrap"
                                                              id="classWrap"
                                                            >
                                                              <dt id="fancyBetHeader">
                                                                <span
                                                                  className="bet-check"
                                                                  id="fancyBetAcceptCheck"
                                                                />
                                                              </dt>
                                                              <dd className="col-btn">
                                                                <a
                                                                  className="btn"
                                                                  id="cancel"
                                                                  style={{
                                                                    cursor:
                                                                      "pointer",
                                                                  }}
                                                                  data-bs-dismiss="modal"
                                                                  aria-label="Close"
                                                                >
                                                                  Cancel
                                                                </a>
                                                              </dd>
                                                              <dd
                                                                className="col-odd"
                                                                id="oddsHeader"
                                                              >
                                                                <ul className="quick-bet-confirm">
                                                                  <li id="runs">
                                                                    44
                                                                  </li>
                                                                  <li
                                                                    className="quick-bet-confirm-title"
                                                                    id="odds"
                                                                  >
                                                                    100
                                                                  </li>
                                                                </ul>
                                                              </dd>
                                                              <dd className="col-stake">
                                                                <a
                                                                  className="icon-minus show-xs"
                                                                  id="stakeDown"
                                                                />
                                                                <input
                                                                  type="number"
                                                                  className=" oddinput ng-valid ng-dirty ng-touched"
                                                                />
                                                                <a
                                                                  className="icon-plus show-xs"
                                                                  id="stakeUp"
                                                                />
                                                              </dd>
                                                              <dd className="col-send">
                                                                <button
                                                                  className="btn-send disable"
                                                                  id="placeBet"
                                                                  style={{
                                                                    cursor:
                                                                      "pointer",
                                                                  }}
                                                                >
                                                                  Place Bets
                                                                </button>
                                                              </dd>
                                                              <dd
                                                                className="col-stake_list"
                                                                id="stakePopupList"
                                                                style={{
                                                                  display:
                                                                    "block",
                                                                }}
                                                              >
                                                                <ul>
                                                                  <li>
                                                                    <a
                                                                      className="btn"
                                                                      style={{
                                                                        cursor:
                                                                          "pointer",
                                                                      }}
                                                                    >
                                                                      1000
                                                                    </a>
                                                                  </li>
                                                                  <li>
                                                                    <a
                                                                      className="btn"
                                                                      style={{
                                                                        cursor:
                                                                          "pointer",
                                                                      }}
                                                                    >
                                                                      5000
                                                                    </a>
                                                                  </li>
                                                                  <li>
                                                                    <a
                                                                      className="btn"
                                                                      style={{
                                                                        cursor:
                                                                          "pointer",
                                                                      }}
                                                                    >
                                                                      10000
                                                                    </a>
                                                                  </li>
                                                                  <li>
                                                                    <a
                                                                      className="btn"
                                                                      style={{
                                                                        cursor:
                                                                          "pointer",
                                                                      }}
                                                                    >
                                                                      25000
                                                                    </a>
                                                                  </li>
                                                                  <li>
                                                                    <a
                                                                      className="btn"
                                                                      style={{
                                                                        cursor:
                                                                          "pointer",
                                                                      }}
                                                                    >
                                                                      50000
                                                                    </a>
                                                                  </li>
                                                                  <li>
                                                                    <a
                                                                      className="btn"
                                                                      style={{
                                                                        cursor:
                                                                          "pointer",
                                                                      }}
                                                                    >
                                                                      100000
                                                                    </a>
                                                                  </li>
                                                                  <li>
                                                                    <a
                                                                      className="btn"
                                                                      style={{
                                                                        cursor:
                                                                          "pointer",
                                                                      }}
                                                                    >
                                                                      200000
                                                                    </a>
                                                                  </li>
                                                                  <li>
                                                                    <a
                                                                      className="btn"
                                                                      style={{
                                                                        cursor:
                                                                          "pointer",
                                                                      }}
                                                                    >
                                                                      500000
                                                                    </a>
                                                                  </li>
                                                                </ul>
                                                              </dd>
                                                            </dl>
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td colSpan={7} className="padd_remo">
                                            <table className="bit-add_Table">
                                              <tbody>
                                                <tr
                                                  className="fancy-quick-tr slip-back "
                                                  style={{
                                                    display: "table-row",
                                                  }}
                                                >
                                                  <td
                                                    className="modal bitBrs1"
                                                    id="exampleModal_new4"
                                                    data-bs-backdrop="false"
                                                    data-bs-keyboard="false"
                                                    tabIndex={-1}
                                                    aria-labelledby="exampleModalLabel4"
                                                    aria-hidden="true"
                                                  >
                                                    <table className="bit-add_Table">
                                                      <tbody className="modal-dialog">
                                                        <tr className="modal-content">
                                                          <td className="modal-body">
                                                            <dl
                                                              className="quick_bet-wrap"
                                                              id="classWrap"
                                                            >
                                                              <dt id="fancyBetHeader">
                                                                <span
                                                                  className="bet-check"
                                                                  id="fancyBetAcceptCheck"
                                                                />
                                                              </dt>
                                                              <dd className="col-btn">
                                                                <a
                                                                  className="btn"
                                                                  id="cancel"
                                                                  style={{
                                                                    cursor:
                                                                      "pointer",
                                                                  }}
                                                                  data-bs-dismiss="modal"
                                                                  aria-label="Close"
                                                                >
                                                                  Cancel
                                                                </a>
                                                              </dd>
                                                              <dd
                                                                className="col-odd"
                                                                id="oddsHeader"
                                                              >
                                                                <ul className="quick-bet-confirm">
                                                                  <li id="runs">
                                                                    44
                                                                  </li>
                                                                  <li
                                                                    className="quick-bet-confirm-title"
                                                                    id="odds"
                                                                  >
                                                                    100
                                                                  </li>
                                                                </ul>
                                                              </dd>
                                                              <dd className="col-stake">
                                                                <a
                                                                  className="icon-minus show-xs"
                                                                  id="stakeDown"
                                                                />
                                                                <input
                                                                  type="number"
                                                                  className=" oddinput ng-valid ng-dirty ng-touched"
                                                                />
                                                                <a
                                                                  className="icon-plus show-xs"
                                                                  id="stakeUp"
                                                                />
                                                              </dd>
                                                              <dd className="col-send">
                                                                <button
                                                                  className="btn-send disable"
                                                                  id="placeBet"
                                                                  style={{
                                                                    cursor:
                                                                      "pointer",
                                                                  }}
                                                                >
                                                                  Place Bets
                                                                </button>
                                                              </dd>
                                                              <dd
                                                                className="col-stake_list"
                                                                id="stakePopupList"
                                                                style={{
                                                                  display:
                                                                    "block",
                                                                }}
                                                              >
                                                                <ul>
                                                                  <li>
                                                                    <a
                                                                      className="btn"
                                                                      style={{
                                                                        cursor:
                                                                          "pointer",
                                                                      }}
                                                                    >
                                                                      1000
                                                                    </a>
                                                                  </li>
                                                                  <li>
                                                                    <a
                                                                      className="btn"
                                                                      style={{
                                                                        cursor:
                                                                          "pointer",
                                                                      }}
                                                                    >
                                                                      5000
                                                                    </a>
                                                                  </li>
                                                                  <li>
                                                                    <a
                                                                      className="btn"
                                                                      style={{
                                                                        cursor:
                                                                          "pointer",
                                                                      }}
                                                                    >
                                                                      10000
                                                                    </a>
                                                                  </li>
                                                                  <li>
                                                                    <a
                                                                      className="btn"
                                                                      style={{
                                                                        cursor:
                                                                          "pointer",
                                                                      }}
                                                                    >
                                                                      25000
                                                                    </a>
                                                                  </li>
                                                                  <li>
                                                                    <a
                                                                      className="btn"
                                                                      style={{
                                                                        cursor:
                                                                          "pointer",
                                                                      }}
                                                                    >
                                                                      50000
                                                                    </a>
                                                                  </li>
                                                                  <li>
                                                                    <a
                                                                      className="btn"
                                                                      style={{
                                                                        cursor:
                                                                          "pointer",
                                                                      }}
                                                                    >
                                                                      100000
                                                                    </a>
                                                                  </li>
                                                                  <li>
                                                                    <a
                                                                      className="btn"
                                                                      style={{
                                                                        cursor:
                                                                          "pointer",
                                                                      }}
                                                                    >
                                                                      200000
                                                                    </a>
                                                                  </li>
                                                                  <li>
                                                                    <a
                                                                      className="btn"
                                                                      style={{
                                                                        cursor:
                                                                          "pointer",
                                                                      }}
                                                                    >
                                                                      500000
                                                                    </a>
                                                                  </li>
                                                                </ul>
                                                              </dd>
                                                            </dl>
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                        <tr className="odd-row">
                                          <td className="team-name valign-middle">
                                            <a className="title">
                                              WI 6 Over Runs ADV{" "}
                                            </a>
                                            <span className="minus-book">
                                              <i className="fa fa-arrow-right" />
                                              0.00
                                            </span>
                                          </td>
                                          <td
                                            className="valign-middle mbl-book"
                                            style={{ position: "relative" }}
                                          >
                                            <button className="book-btn btn btn-primary ">
                                              Book
                                            </button>
                                          </td>
                                          <td
                                            id="fancy1012022730191742154"
                                            className="lay-1 count"
                                          >
                                            <a>
                                              44
                                              <span>100</span>
                                            </a>
                                          </td>
                                          <td className="back-1 count">
                                            <a>
                                              46
                                              <span>100</span>
                                            </a>
                                          </td>
                                          <td className="min-max hidden-xs">
                                            100-50000
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div
                            className="tab-pane "
                            id="nav-fancy_d"
                            role="tabpanel"
                            aria-labelledby="nav-fancy-tab_d"
                          >
                            <div className="card  fancy-card">
                              <div className="card-header card-accrdn card-fancybet">
                                <div className="card-body p-0">
                                  <div className="table-responsive">
                                    <table className="table table-hover tbl-bets fancy-bet p-rltv">
                                      <thead>
                                        <tr>
                                          <th className="minwidth" colSpan={2}>
                                            <marquee
                                              className="pull-right fancy-marquee"
                                              direction="scroll"
                                            >
                                              <strong />
                                            </marquee>
                                          </th>
                                          <th className="lay-1">No</th>
                                          <th className="back-1">Yes</th>
                                          <th className="hidden-xs">Min/Max</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr className="odd-row">
                                          <td className="team-name valign-middle">
                                            <a className="title">
                                              WI 6 Over Runs ADV{" "}
                                            </a>
                                            <span className="minus-book">
                                              <i className="fa fa-arrow-right" />
                                              0.00
                                            </span>
                                          </td>
                                          <td
                                            className="valign-middle mbl-book"
                                            style={{ position: "relative" }}
                                          >
                                            <button className="book-btn btn btn-primary ">
                                              Book
                                            </button>
                                          </td>
                                          <td
                                            id="fancy1012022730191742154"
                                            className="lay-1 count"
                                          >
                                            <a>
                                              44
                                              <span>100</span>
                                            </a>
                                          </td>
                                          <td className="back-1 count">
                                            <a>
                                              46
                                              <span>100</span>
                                            </a>
                                          </td>
                                          <td className="min-max hidden-xs">
                                            100-50000
                                          </td>
                                        </tr>
                                        <tr className="odd-row">
                                          <td className="team-name valign-middle">
                                            <a className="title">
                                              WI 6 Over Runs ADV{" "}
                                            </a>
                                            <span className="minus-book">
                                              <i className="fa fa-arrow-right" />
                                              0.00
                                            </span>
                                          </td>
                                          <td
                                            className="valign-middle mbl-book"
                                            style={{ position: "relative" }}
                                          >
                                            <button className="book-btn btn btn-primary ">
                                              Book
                                            </button>
                                          </td>
                                          <td
                                            id="fancy1012022730191742154"
                                            className="lay-1 count"
                                          >
                                            <a>
                                              44
                                              <span>100</span>
                                            </a>
                                          </td>
                                          <td className="back-1 count">
                                            <a>
                                              46
                                              <span>100</span>
                                            </a>
                                          </td>
                                          <td className="min-max hidden-xs">
                                            100-50000
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div
                            className="tab-pane "
                            id="nav-lineMarket_d"
                            role="tabpanel"
                            aria-labelledby="nav-lineMarket-tab_d"
                          >
                            <div className="card  fancy-card">
                              <div className="card-header card-accrdn card-fancybet">
                                <div className="card-body p-0">
                                  <div className="table-responsive">
                                    <table className="table table-hover tbl-bets fancy-bet p-rltv">
                                      <thead>
                                        <tr>
                                          <th className="minwidth" colSpan={2}>
                                            <marquee
                                              className="pull-right fancy-marquee"
                                              direction="scroll"
                                            >
                                              <strong />
                                            </marquee>
                                          </th>
                                          <th className="lay-1">No</th>
                                          <th className="back-1">Yes</th>
                                          <th className="hidden-xs">Min/Max</th>
                                        </tr>
                                      </thead>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div
                            className="tab-pane "
                            id="nav-ballByball_d"
                            role="tabpanel"
                            aria-labelledby="nav-ballByball-tab_d"
                          >
                            <div className="card  fancy-card">
                              <div className="card-header card-accrdn card-fancybet">
                                <div className="card-body p-0">
                                  <div className="table-responsive">
                                    <table className="table table-hover tbl-bets fancy-bet p-rltv">
                                      <thead>
                                        <tr>
                                          <th className="minwidth" colSpan={2}>
                                            <marquee
                                              className="pull-right fancy-marquee"
                                              direction="scroll"
                                            >
                                              <strong />
                                            </marquee>
                                          </th>
                                          <th className="lay-1">No</th>
                                          <th className="back-1">Yes</th>
                                          <th className="hidden-xs">Min/Max</th>
                                        </tr>
                                      </thead>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div
                            className="tab-pane "
                            id="nav-meterMarket_d"
                            role="tabpanel"
                            aria-labelledby="nav-meterMarket-tab_d"
                          >
                            <div className="card  fancy-card">
                              <div className="card-header card-accrdn card-fancybet">
                                <div className="card-body p-0">
                                  <div className="table-responsive">
                                    <table className="table table-hover tbl-bets fancy-bet p-rltv">
                                      <thead>
                                        <tr>
                                          <th className="minwidth" colSpan={2}>
                                            <marquee
                                              className="pull-right fancy-marquee"
                                              direction="scroll"
                                            >
                                              <strong />
                                            </marquee>
                                          </th>
                                          <th className="lay-1">No</th>
                                          <th className="back-1">Yes</th>
                                          <th className="hidden-xs">Min/Max</th>
                                        </tr>
                                      </thead>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div
                            className="tab-pane "
                            id="nav-khadoMarket_d"
                            role="tabpanel"
                            aria-labelledby="nav-khadoMarket-tab_d"
                          ></div>
                        </div>
                      </div>
                      <div
                        className="tab-pane "
                        id="sportsBook_d"
                        role="tabpanel"
                        aria-labelledby="sportsBook-tab_d"
                      >
                        <nav className="fancyBet-fabList revert">
                          <div
                            className="nav nav-tabs justify-content-center outer_navtablist"
                            id="nav-tab"
                            role="tablist"
                          >
                            <button
                              className="nav-link active"
                              id="nav-s_all-tab"
                              data-bs-toggle="tab"
                              data-bs-target="#nav-s_all"
                              type="button"
                              role="tab"
                              aria-controls="nav-home"
                              aria-selected="true"
                            >
                              All
                            </button>
                            <button
                              className="nav-link"
                              id="nav-match-tab"
                              data-bs-toggle="tab"
                              data-bs-target="#nav-match"
                              type="button"
                              role="tab"
                              aria-controls="nav-profile"
                              aria-selected="false"
                            >
                              Match
                            </button>
                            <button
                              className="nav-link"
                              id="nav-oddEven-tab"
                              data-bs-toggle="tab"
                              data-bs-target="#nav-oddEven"
                              type="button"
                              role="tab"
                              aria-controls="nav-contact"
                              aria-selected="false"
                            >
                              Odd/Even
                            </button>
                            <button
                              className="nav-link"
                              id="nav-batsman-tab"
                              data-bs-toggle="tab"
                              data-bs-target="#nav-batsman"
                              type="button"
                              role="tab"
                              aria-controls="nav-contact"
                              aria-selected="false"
                            >
                              Batsman
                            </button>
                            <button
                              className="nav-link"
                              id="nav-bowler-tab"
                              data-bs-toggle="tab"
                              data-bs-target="#nav-bowler"
                              type="button"
                              role="tab"
                              aria-controls="nav-contact"
                              aria-selected="false"
                            >
                              Bowler
                            </button>
                            <button
                              className="nav-link"
                              id="nav-extra-tab"
                              data-bs-toggle="tab"
                              data-bs-target="#nav-extra"
                              type="button"
                              role="tab"
                              aria-controls="nav-contact"
                              aria-selected="false"
                            >
                              Extra
                            </button>
                          </div>
                        </nav>
                        <div
                          className="tab-content"
                          id="nav-tabContent"
                          style={{ height: "auto" }}
                        ></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
