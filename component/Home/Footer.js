import React from "react";

export function Footer() {
  return (
    <div className="inner-footer">
      <div className="support-wrap">
        <h3>Support</h3>
        <dl className="support-app">
          <dt>
            <i
              aria-hidden="true"
              className="fa fa-phone"
              style={{ fontSize: "18px", lineHeight: "24px" }}
            />
          </dt>
          <dd>
            <a href="tel:">
              <span>Calling</span> 999999999
            </a>
          </dd>
        </dl>
        <dl className="support-app">
          <dt>
            <img src="/img/test.gif" title="WhatsApp" />
          </dt>
          <dd>
            <a href>
              <span>WhatsApp</span> +91 999999999
            </a>
          </dd>
        </dl>
      </div>
      <div className="support-wrap" bis_skin_checked={1}>
        <dl className="support-mail">
          <a className="rules-btn-home">Privacy Policy</a>
          <a className="rules-btn-home arrow">KYC</a>
          <a className="rules-btn-home arrow">Terms and Conditions</a>
          <a className="rules-btn-home arrow">Rules and Regulations</a>
          <a className="rules-btn-home arrow">Responsible Gambling</a>
        </dl>
      </div>
    </div>
  );
}
