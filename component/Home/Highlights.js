import ContextHook from "@helper/ContextHook";
import { postData } from "@services/swr";
import { useRouter } from "next/router";
import React, { useContext } from "react";
import useSWR from "swr";

export function Highlights() {
  const { data: eventList } = useSWR("getAllSportEvents");
  const { data: activeSportId } = useSWR("activeSports");
  const { data: sports } = useSWR("getAllSports");
  const { setLoginModal } = useContext(ContextHook.LoginContext);
  const { userDetails } = useContext(ContextHook.LoginDetails);
  const router = useRouter();

  const addToPinHandler = async(item) => {
    if (userDetails?.isAuthenticate) {
      const resp = await postData(["pinMatch", item]);
    } else {
      setLoginModal(true);
    }
  };

  return (
    <div className="game-highlight-wrap">
      <h3>Highlights</h3>
      <ul className="nav nav-tabs" id="myTab" role="tablist">
        {sports?.data?.mapWithKey((sport, key) => (
          <li className="nav-item" role="presentation" key={key}>
            <button
              className={
                "nav-link " +
                (sport?.sport_id === +activeSportId ? "active" : "")
              }
              data-bs-toggle="tab"
              type="button"
              role="tab"
              aria-selected="true"
              onClick={() => router.push("/sports/" + sport?.sport_id)}
            >
              {sport?.sports_name}
            </button>
          </li>
        ))}
      </ul>
      <div
        className="tab-content game-menu-tab"
        id="myTabContent"
        bis_skin_checked={1}
      >
        <div
          className="tab-pane fade show active"
          id="cricket"
          role="tabpanel"
          aria-labelledby="cricket-tab"
          bis_skin_checked={1}
        >
          <table className="table table-hover">
            <thead>
              <tr>
                <th className="view-mobile" colSpan={2} />
                <th className="text-center">1</th>
                <th className="text-center">X</th>
                <th className="text-center">2</th>
                <th className="text-center" />
              </tr>
            </thead>
            <tbody className="_tbody">
              {eventList?.data?.mapWithKey((list, key) => (
                <tr kry={key}>
                  <td>
                    <a href="index2.html" className="match-name">
                      <span>{list?.event_name}</span>
                    </a>
                    {/* <span className="in_play">In-Play</span> */}
                    <p className="match-time">{list?.open_date_ist}</p>
                  </td>
                  <td />
                  {/* <td>
            <span className="game-fancy mr-1">F</span>
            <span className="game-sportbook">S</span>
          </td> */}
                  <td className="count">
                    <span className="back">1.53</span>
                    <span className="lay">1.6</span>
                  </td>
                  <td className="count">
                    <span className="back">0.0</span>
                    <span className="lay">0.0</span>
                  </td>
                  <td className="count">
                    <span className="back">0</span>
                    <span className="lay">0</span>
                  </td>
                  <td className="pl-0">
                    <div
                      className="add-pin"
                      onClick={() => addToPinHandler(list)}
                    />
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
