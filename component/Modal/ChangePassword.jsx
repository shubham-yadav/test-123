import { FloatingField } from "@component/Common/FloatingField";
import React from "react";
import { Modal } from "react-bootstrap";

export default function ChangePassword(props) {
  const { open, toggle } = props;
  return (
    <Modal show={open} centered onHide={() => toggle(false)} className="anvChangePass_modal">
      <div className="modal-content">
        <div className="modal-header account-detail-head">
          <h5 className="modal-title" id="exampleModalLabel">
            Change Password
          </h5>
          <button
            onClick={() => toggle(false)}
            type="button"
            className="close_eyeModal"
            data-bs-dismiss="modal"
            aria-label="Close"
          >
            <i className="fa-solid fa-x" />
          </button>
        </div>
        <form className="ps-changeFrm ">
          <div className="modal-body modBody_contain">
            <div className="anv_inRow">
            <div className="anvCol_bx">
              <div className="form-group">
                {/* <label htmlFor="meta_keywords">
                  Old Password <span className="asterisk_input" />
                </label> */}
                <FloatingField
                  className="form-control ng-pristine ng-invalid ng-touched anv_innrInput"
                  name="oldPassword"
                  label="Old Password"
                  placeholder="Old Password.."
                  required
                  type="password"
                />
              </div>
            </div>
            <div className="anvCol_bx">
              <div className="form-group">
                {/* <label htmlFor="meta_keywords">
                  New Password <span className="asterisk_input" />
                </label> */}
                <FloatingField
                  className="form-control ng-pristine ng-invalid ng-touched anv_innrInput"
                  label="New Password"
                  name="newPassword"
                  pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d!@#$%^&]{8,}$"
                  placeholder="New Password.."
                  required
                  type="password"
                />
              </div>
            </div>
            <div className="anvCol_bx">
              <div className="form-group">
                {/* <label htmlFor="meta_keywords">
                  Confirm Password <span className="asterisk_input" />
                </label> */}
                <FloatingField
                  className="form-control ng-pristine ng-invalid ng-touched anv_innrInput"
                  label="Confirm Password"
                  name="confirmPass"
                  placeholder="Confirm Password.."
                  required
                  type="password"
                />
              </div>
            </div>
            </div>
            
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-primary cst_yesBtn">
              Yes
            </button>
            <button
              onClick={() => toggle(false)}
              type="button"
              className="btn btn-secondary cst_noBtn"
              data-bs-dismiss="modal"
            >
              No
            </button>
          </div>
        </form>
      </div>
    </Modal>
  );
}
