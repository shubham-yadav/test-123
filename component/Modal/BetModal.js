import React from "react";

export default function BetModal() {
  return (
    <>
      {/* modal1 */}
      <div
        className="modal fade"
        id="exampleModal"
        tabIndex={-1}
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Modal title
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              />
            </div>
            <div className="modal-body">...</div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-bs-dismiss="modal"
              >
                Close
              </button>
              <button type="button" className="btn btn-primary">
                Save changes
              </button>
            </div>
          </div>
        </div>
      </div>
      {/* modal2 */}
      <div
        className="modal fade"
        id="eyeModal"
        tabIndex={-1}
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header account-detail-head">
              <h5 className="modal-title" id="exampleModalLabel">
                Rolling Commission
              </h5>
              <button
                type="button"
                className="close_eyeModal"
                data-bs-dismiss="modal"
                aria-label="Close"
              >
                <i className="fa-solid fa-x" />
              </button>
            </div>
            <div className="modal-body">
              <div className="col-md-12">
                <div className="view-rolling row">
                  <label className="col-md-3 col-3 col-form-label">Fancy</label>
                  <div className="col-md-8 col-8 col-form-label">
                    <span>0</span>
                  </div>
                </div>
                <div className="view-rolling row">
                  <label className="col-md-3 col-3 col-form-label">Matka</label>
                  <div className="col-md-8 col-8 col-form-label">
                    <span>0</span>
                  </div>
                </div>
                <div className="view-rolling row">
                  <label className="col-md-3 col-3 col-form-label">
                    Casino
                  </label>
                  <div className="col-md-8 col-8 col-form-label">
                    <span>0</span>
                  </div>
                </div>
                <div className="view-rolling row">
                  <label className="col-md-3 col-3 col-form-label">
                    Binary
                  </label>
                  <div className="col-md-8 col-8 col-form-label">
                    <span>0</span>
                  </div>
                </div>
                <div className="view-rolling row">
                  <label className="col-md-3 col-3 col-form-label">
                    Sportbook
                  </label>
                  <div className="col-md-8 col-8 col-form-label">
                    <span>0</span>
                  </div>
                </div>
                <div className="view-rolling row">
                  <label className="col-md-3 col-3 col-form-label">
                    Bookmaker
                  </label>
                  <div className="col-md-8 col-8 col-form-label">
                    <span>0</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* modal3 */}
      {/* open_bet modal */}
      <div
        className="modal fade"
        id="openBet_modal"
        tabIndex={-1}
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog  modal-fullscreen">
          <div className="modal-content">
            <div className="modal-header openBet_header">
              <h5 className="modal-title" id="exampleModalLabel">
                Open Bets
              </h5>
              <a data-bs-dismiss="modal" aria-label="Close">
                <i className="fa-regular fa-x" />
              </a>
              {/* <button type="button" class="" data-bs-dismiss="modal" aria-label="Close"></button> */}
            </div>
            <div className="modal-body"></div>
          </div>
        </div>
      </div>
      {/* betSetting modal */}
      <div
        className="modal fade"
        id="betSetting_modal"
        tabIndex={-1}
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog ">
          <div className="modal-content">
            <div className="modal-header openBet_header">
              <h5 className="modal-title" id="exampleModalLabel">
                <i className="fa-regular fa-gear" /> Setting
              </h5>
              <a data-bs-dismiss="modal" aria-label="Close">
                <i className="fa-regular fa-x" />
              </a>
              {/* <button type="button" class="" data-bs-dismiss="modal" aria-label="Close"></button> */}
            </div>
            <h3 className="siteContent_h3">Stake</h3>
            <div className="modal-body">
              <dl
                className="setting-block stake-setting"
                id="editCustomizeStakeList"
              >
                <dt>Quick Stakes</dt>
                <dd>
                  <input
                    _ngcontent-xry-c0
                    name="stack_value"
                    type="text"
                    className="ng-untouched ng-pristine ng-valid"
                  />
                </dd>
                <dd>
                  <input
                    name="stack_value"
                    type="text"
                    className="ng-untouched ng-pristine ng-valid"
                  />
                </dd>
                <dd>
                  <input
                    name="stack_value"
                    type="text"
                    className="ng-untouched ng-pristine ng-valid"
                  />
                </dd>
                <dd>
                  <input
                    _ngcontent-xry-c0
                    name="stack_value"
                    type="text"
                    className="ng-untouched ng-pristine ng-valid"
                  />
                </dd>
                <dd>
                  <input
                    name="stack_value"
                    type="text"
                    className="ng-untouched ng-pristine ng-valid"
                  />
                </dd>
                <dd>
                  <input
                    name="stack_value"
                    type="text"
                    className="ng-untouched ng-pristine ng-valid"
                  />
                </dd>
                <dd>
                  <input
                    name="stack_value"
                    type="text"
                    className="ng-untouched ng-pristine ng-valid"
                  />
                </dd>
                <dd>
                  <input
                    name="stack_value"
                    type="text"
                    className="ng-untouched ng-pristine ng-valid"
                  />
                </dd>
                <dd className="col-stake_edit">
                  <a className="btn-send ui-link" href="/" id="save">
                    Save
                  </a>
                </dd>
              </dl>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
