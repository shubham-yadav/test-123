import { useContext } from "react";
import { Modal } from "react-bootstrap";
import ContextHook from "@helper/ContextHook";
import { Login } from "@component/Login";

export default function LoginModal() {
  const { isLoginModal, setLoginModal } = useContext(ContextHook.LoginContext);

  return (
    <Modal show={isLoginModal} onHide={() => setLoginModal(false)}>
      <div className="screen">
        <div>
          <div className="login_logo">
            <img src="/img/R444.png" alt="R444_logo" />
          </div>
          <Login formClass="login_modal_form" />
        </div>
      </div>
    </Modal>
  );
}
