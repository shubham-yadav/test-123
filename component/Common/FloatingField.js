import { FloatingLabel, Form } from "react-bootstrap";

export const FloatingField = (props) => {
  const { error, label, focus, labelClass } = props;
  const invalid = !!(focus && error);
  const valid = !!(focus && !error);
  return (
    <>
      <FloatingLabel
        label={label}
        className={(labelClass ? labelClass : "") + " " + " _inInput_fx anv_inInput_fx"}
      >
        <Form.Control
          controlId="floatingInput"
          autoComplete="off"
          isInvalid={invalid}
          valid={valid}
          {...props}
        />
        {error && <p className="invalid-feedback mb-0  anvTooltip">{error}</p>}
      </FloatingLabel>
    </>
  );
};
