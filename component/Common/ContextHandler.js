import ContextHook from "@helper/ContextHook";
import { useSession } from "next-auth/react";
import React, { useState } from "react";

export default function ContextHandler({ children }) {
  const [isLoginModal, setLoginModal] = useState(false);
  const { data: session, status: fetchStatus } = useSession();
  return (
    <ContextHook.LoginDetails.Provider
      value={{
        userDetails: session?.user?.data
          ? {
              ...JSON.parse(session?.user?.data),
              isAuthenticate:
                session?.user?.isAuthenticate === "true" ? true : false,
            }
          : null,
        fetchStatus,
      }}
    >
      <ContextHook.LoginContext.Provider
        value={{ isLoginModal, setLoginModal }}
      >
        {children};
      </ContextHook.LoginContext.Provider>
    </ContextHook.LoginDetails.Provider>
  );
}
