import React, { useCallback } from "react";
import useSWR from "swr";
import { apiEndPoints, methodType, performRequest } from "./_apiConfig";
import AppConfig from "./_apiConfig/apiConfig";

export default function useHome(path, req) {
  const APIURL = req?.id
    ? apiEndPoints[path] + "/" + req?.id
    : apiEndPoints[path];
  const fetchData = async (data) => {
    const response = await performRequest(methodType.GET, APIURL, data, true);
    return response;
  };
  const { data, error, isLoading, mutate } = useSWR(APIURL, fetchData);

  return {
    user: data,
    isLoading,
    isError: error,
    fetchData,
    mutate,
  };
}
