export const methodType = {
  PUT: "put",
  GET: "get",
  POST: "post",
  DELETE: "delete",
  PATCH: "patch",
};

export const apiEndPoints = {
  userLogin: "login",
  userChangePassword: "/user/change_password",
  pinMatch: "/pin_match",
  coreIndustry: "/core/industry/",
  coreCategory: "/core/question-category/",
  coreQuestionnaire: "core/questionnaire/",
  coreSection: "core/section/",
  inplayEvents: "inplay_events",
  getAllSports: "get_all_sports",
  getAllSportLeagues: "get_all_sport_leagues",
  getAllLeaguesEvents: "get_all_league_events",
  getAllSportEvents: "get_all_sport_events",
};
