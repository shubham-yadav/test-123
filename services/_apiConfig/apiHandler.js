import axios from "axios";
import { AppConfig } from "./apiConfig";
import { getStorage, clearStorage } from "@/helper/storage";
import { methodType } from "./apiEndPoints";

axios.interceptors.response.use(
  (response) => {
    const { data } = response;
    return data;
  },
  (error) => {
    const {
      response: { data },
      response,
    } = error;
    console.log("response :::::::: ", response);
    // if (response.status === 401) {
    //   clearStorage();
    //   return Promise.reject(error);
    // }
    if (data) {
      return data;
    } else {
      return Promise.reject(error);
    }
  }
);

export const performRequest = async (method, url, data = {}, token) => {
  url = url.replaceAll("#", "%23");
  const config = {
    method,
    url,
    baseURL: AppConfig.baseURL,
  };

  if (
    method === methodType.PUT ||
    method === methodType.PATCH ||
    method === methodType.POST ||
    method === methodType.DELETE
  ) {
    config.data = data;
  }

  if (method === methodType.GET) {
    config.params = data;
  }

  config.headers = {
    "Content-Type": "application/json; charset=utf-8",
  };

  if (token) {
    config.headers.Authorization = `Token ${token}`;
  }
  return axios.request(config);
};
