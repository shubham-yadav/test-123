import { performRequest, apiEndPoints, methodType } from "./_apiConfig";

export const getTaskProgress = (data = {}) => {
  return performRequest(methodType.GET, apiEndPoints.coreAttempt, data, true)
    .then((response) => {
      return response;
    })
    .catch((error) => {
      return error;
    });
};

export const addTaskProgress = (data = {}) => {
  return performRequest(methodType.POST, apiEndPoints.coreAttempt, data)
    .then((response) => {
      return response;
    })
    .catch((error) => {
      return error;
    });
};

export const deleteTaskProgress = (id) => {
  return performRequest(methodType.DELETE, apiEndPoints.coreAttempt + id + "/")
    .then((response) => {
      return response;
    })
    .catch((error) => {
      return error;
    });
};

export const updateTaskProgress = (data = {}) => {
  return performRequest(
    methodType.PUT,
    apiEndPoints.coreAttempt + data?.id + "/",
    data,
    true
  )
    .then((response) => {
      return response;
    })
    .catch((error) => {
      return error;
    });
};

export const getPreviewDownload = (data) => {
  return performRequest(
    methodType.GET,
    apiEndPoints.coreAttempt + data?.id + "/download_pdf",
    data,
    true
  )
    .then((response) => {
      return response;
    })
    .catch((error) => {
      return error;
    });
};

export const getPreviewAttemptAnswer = (data) => {
  return performRequest(
    methodType.GET,
    apiEndPoints.coreAttempt + data?.id + "/preview_attempt_answers",
    data,
    true
  )
    .then((response) => {
      return response;
    })
    .catch((error) => {
      return error;
    });
};
