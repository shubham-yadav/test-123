import useSWR from "swr";

const useAPISWR = (url) => {
  const { data, error, mutate } = useSWR(url);

  const getData = async () => {
    try {
      const response = await fetch(url);

      if (!response.ok) {
        throw new Error("Request failed");
      }

      const data = await response.json();
      mutate(data);
    } catch (error) {
      console.error("Error:", error);
    }
  };

  const postData = async (payload) => {
    try {
      const response = await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(payload),
      });

      if (!response.ok) {
        throw new Error("Request failed");
      }

      mutate();
    } catch (error) {
      console.error("Error:", error);
    }
  };

  const putData = async (payload) => {
    try {
      const response = await fetch(url, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(payload),
      });

      if (!response.ok) {
        throw new Error("Request failed");
      }

      mutate();
    } catch (error) {
      console.error("Error:", error);
    }
  };

  return {
    data,
    error,
    getData,
    postData,
    putData,
  };
};

export default useAPISWR;
