import React, { useCallback } from "react";
import useSWR from "swr";
import { apiEndPoints, methodType, performRequest } from "./_apiConfig";
import AppConfig from "./_apiConfig/apiConfig";
import { getStorage } from "@helper/storage";

export const fetchData = async (path, data) => {
  const APIURL = data?.id
    ? apiEndPoints[path] + "/" + data?.id
    : apiEndPoints[path];
  const response = await performRequest(
    methodType.GET,
    AppConfig.baseURL + APIURL
  );
  // const jsonData = await res.json();
  return response;
};

export const postData = async (payload) => {
  const token = await getStorage();
  const data = payload[1],
    path = payload[0];
  const APIURL = data?.id
    ? apiEndPoints[path] + "/" + data?.id
    : apiEndPoints[path];
  const response = await performRequest(
    methodType.POST,
    AppConfig.baseURL + APIURL,
    data,
    token
  );
  return response;
};

export default function swr(path) {
  const fetchData = useCallback(async (data) => {
    const APIURL = data?.id
      ? apiEndPoints[path] + "/" + data?.id
      : apiEndPoints[path];
    const response = await performRequest(methodType.GET, APIURL, data, true);
    return response;
  }, []);

  const { data, isLoading, error, isValidating, mutate } = useSWR(
    path,
    fetchData
  );
  return { fetchData, data, isLoading, error, isValidating, mutate };
}
