/** @type {import('next').NextConfig} */
const nextConfig = {
  // async rewrites() {
  //   return [
  //     {
  //       source: "/sports\\?id=2&data=test",
  //       destination: "/sports/[id]",
  //     },
  //   ];
  // },
  output: 'build',
  distDir: 'build',
  images: {
    unoptimized: true
  },
  reactStrictMode: false,
};

module.exports = nextConfig;
