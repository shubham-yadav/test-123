import { configRoute } from "@helper/constant";
import { NextResponse } from "next/server";

export async function middleware(request) {
  // checking with session-token is exist in cookies to get user authentication status
  const isAuthenticated = !!request.cookies
    .getAll()
    .find((item) => item?.name === "next-auth.session-token")?.value;
  if (!isAuthenticated) {
    // checking with constant file private route if that's match with curretn route
    const is = configRoute.private.some((item) => {
      return request.nextUrl.pathname.startsWith(item);
    });
    if (!!is) {
      return NextResponse.redirect(new URL("/sports", request.url));
    }
  }
  if (request.nextUrl.pathname.startsWith("/inplay")) {
    return NextResponse.rewrite(new URL("/Inplay", request.url));
  }
  if (request.nextUrl.pathname.startsWith("/inplay/")) {
    return NextResponse.rewrite(new URL("/Inplay/:id", request.url));
  }
  // if (request.nextUrl.pathname.startsWith("/ssssssss")) {
  //   return NextResponse.rewrite(new URL("/inplay", request.url));
  // }
}
