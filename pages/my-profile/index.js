import { HomeComponent } from "@/component";
import ChangePassword from "@component/Modal/ChangePassword";
import Head from "next/head";
import { useState } from "react";
import { Button } from "react-bootstrap";

export default function Home() {
  const [isShow, setIsShow] = useState(false);
  return (
    <>
      <Head>
        <title>Home page</title>
        <meta name="description" content="Created by R444" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="mob_views">
        <div className="divider-top">
          <div className="col-sm-12 col-md-12 p-mobile-1">
            <div className="card card-accent-primary">
              <div className="card-header account-detail-head">
                Account Details
              </div>
              <div className="card-body account-detail-body">
                <div className="bd-example">
                  <dl className="row">
                    <dt className="col-md-3">Name</dt>
                    <dd className="col-md-9">rrr</dd>
                    <dt className="col-md-3">Commission</dt>
                    <dd className="col-md-9">1</dd>
                    <dt className="col-md-3">Rolling Commission</dt>
                    <dd className="col-md-9">
                      <a
                        className="eye-edit"
                        data-bs-toggle="modal"
                        data-bs-target="#eyeModal"
                        style={{ cursor: "pointer" }}
                      >
                        <i className="fa fa-eye fa-lg" />
                      </a>
                    </dd>
                    <dt className="col-md-3">Exposure Limit</dt>
                    <dd className="col-md-9">1000</dd>
                    <dt className="col-md-3">Mobile Number</dt>
                    <dd className="col-md-9">000</dd>
                    <dt className="col-md-3">Password</dt>
                    <dd className="col-md-9">
                      <span className="mrg-3">*********</span>
                      <a
                        className="edit-password"
                        data-bs-toggle="modal"
                        data-bs-target="#editPassword"
                        style={{ cursor: "pointer" }}
                      >
                        {" "}
                        Edit
                        <i className="fa-solid fa-pen-to-square" />
                      </a>
                    </dd>
                  </dl>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="hiLight">
        <div className="container-fluid" bis_skin_checked={1}>
          <div className="row" bis_skin_checked={1}>
            <HomeComponent.SportsList />
            <div className="maincol ">
              <div className="divider-top">
                <div className="col-sm-12 col-md-12 p-mobile-1">
                  <div className="card card-accent-primary">
                    <div className="card-header account-detail-head">
                      Account Details
                    </div>
                    <div className="card-body account-detail-body">
                      <div className="bd-example">
                        <dl className="row">
                          <dt className="col-md-3">Name</dt>
                          <dd className="col-md-9">rrr</dd>
                          <dt className="col-md-3">Commission</dt>
                          <dd className="col-md-9">1</dd>
                          <dt className="col-md-3">Rolling Commission</dt>
                          <dd className="col-md-9">
                            <a
                              className="eye-edit"
                              data-bs-toggle="modal"
                              data-bs-target="#eyeModal"
                              style={{ cursor: "pointer" }}
                            >
                              <i className="fa fa-eye fa-lg" />
                            </a>
                          </dd>
                          <dt className="col-md-3">Exposure Limit</dt>
                          <dd className="col-md-9">1000</dd>
                          <dt className="col-md-3">Mobile Number</dt>
                          <dd className="col-md-9">000</dd>
                          <dt className="col-md-3">Password</dt>
                          <dd className="col-md-9">
                            <span className="mrg-3">*********</span>
                            <button
                              onClick={()=>setIsShow(true)}
                              className="edit-password"
                              data-bs-toggle="modal"
                              data-bs-target="#editPassword"
                              style={{ cursor: "pointer" }}
                            >
                              {" "}
                              Edit
                              <i className="fa-solid fa-pen-to-square" />
                            </button>
                          </dd>
                        </dl>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <HomeComponent.BetList />
          </div>
        </div>
        <ChangePassword open={isShow} toggle={(s) => setIsShow(s)} />
      </section>
    </>
  );
}
