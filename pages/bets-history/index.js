import { HomeComponent } from "@/component";
import Head from "next/head";

export default function Home() {
  return (
    <>
      <Head>
        <title>Home page</title>
        <meta name="description" content="Created by R444" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <section className="bet_historySec">
        <div className="container-fluid" bis_skin_checked={1}>
          <div className="row" bis_skin_checked={1}>
            <HomeComponent.SportsList />
            <div className="maincol ">
              <div className="divider-top">
                <div className="col-sm-12 col-md-12 p-mobile-1">
                  <div className="card card-report card-accent-primary">
                    <div className="card-body row p-0">
                      <div className="col-md-12 betHistory">
                        <div className="row">
                          <div className="col-md-12 row">
                            <div className="col-md-4 col-6">
                              <div className="form-group frm_before">
                                <select className="form-control ng-valid ng-dirty ng-touched ">
                                  <option selected disabled value>
                                    Data Source
                                  </option>
                                  <option value="LIVE ">LIVE DATA</option>
                                  <option value="BACKUP ">BACKUP DATA</option>
                                  <option value="OLD ">OLD DATA</option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-4 col-6">
                              <div className="form-group frm_before">
                                <select className="form-control ng-valid ng-dirty ng-touched ">
                                  <option selected disabled value>
                                    Select Sport
                                  </option>
                                  <option value={1}>Cricket </option>
                                  <option value={2}>Tennis </option>
                                  <option value={3}>Soccer </option>
                                  <option value={4}>Casino </option>
                                  <option value={5}>Horse Racing </option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-4 col-6">
                              <div className="form-group frm_before">
                                <select className="form-control ng-valid ng-dirty ng-touched ">
                                  <option selected disabled value>
                                    Select Type
                                  </option>
                                  <option value="Settle">Settle </option>
                                  <option value="UnSettle">UnSettle </option>
                                  <option value="Void">Void </option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-4 col-6">
                              <div className="form-group frm_before">
                                <input
                                  className="form-control"
                                  id="datepicker"
                                  defaultValue="17-08-2022"
                                />
                              </div>
                            </div>
                            <div className="col-md-4 col-6">
                              <div className="form-group frm_before">
                                <input
                                  className="form-control"
                                  id="datepicker2"
                                  defaultValue="17-08-2022"
                                />
                              </div>
                            </div>
                            <div className="col-md-4 col-6">
                              <div className="form-group">
                                <button
                                  className="get_historyBtn"
                                  type="button"
                                >
                                  <strong>Get History</strong>
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-sm-12 col-md-12 p-mobile-1">
                  <div className="card  card-accent-primary">
                    <div className="card-header account-detail-head">
                      {" "}
                      Bet History{" "}
                    </div>
                    <div className="card-body account-stat-body">
                      <div className="row">
                        <div className="col-md-12 p-mobile-0">
                          <div className="table-responsive">
                            <div
                              id="DataTables_Table_0_wrapper"
                              className="dataTables_wrapper"
                            >
                              <div
                                className="dataTables_length"
                                id="DataTables_Table_0_length"
                              >
                                <label>
                                  Show
                                  <select
                                    name="DataTables_Table_0_length"
                                    aria-controls="DataTables_Table_0"
                                    className
                                  >
                                    <option value={10}>10</option>
                                    <option value={25}>25</option>
                                    <option value={50}>50</option>
                                    <option value={100}>100</option>
                                  </select>{" "}
                                  entries
                                </label>
                              </div>
                              <table className="table table-bordered dataTable">
                                <thead className="text-center">
                                  <tr>
                                    <th className="sorting_desc">Sport Name</th>
                                    <th className="sorting">Event Name</th>
                                    <th className="sorting">Market Name</th>
                                    <th className="sorting">Selection </th>
                                    <th className="sorting">Type</th>
                                    <th className="sorting">Odds Req.</th>
                                    <th className="sorting">Stake</th>
                                    <th className="sorting">Place Time</th>
                                    <th className="sorting">Matched Time</th>
                                  </tr>
                                </thead>
                                <tfoot>
                                  <tr>
                                    <td
                                      className="no-data-available"
                                      colSpan={9}
                                      rowSpan={1}
                                    >
                                      No data!
                                    </td>
                                  </tr>
                                </tfoot>
                              </table>
                              <div
                                className="dataTables_info"
                                id="DataTables_Table_0_info"
                                role="status"
                                aria-live="polite"
                              >
                                Showing 0 to 0 of 0 entries
                              </div>
                              <div className="dataTables_paginate paging_full_numbers">
                                <a className="paginate_button first disabled">
                                  First
                                </a>
                                <a className="paginate_button previous disabled">
                                  Previous
                                </a>
                                <a className="paginate_button next  disabled">
                                  Next
                                </a>
                                <a className="paginate_button last  disabled">
                                  Last
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <HomeComponent.BetList />
          </div>
        </div>
      </section>
    </>
  );
}
