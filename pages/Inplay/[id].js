import Head from "next/head";
import { HomeComponent } from "@/component";
import { useRouter } from "next/router";

export default function InplayDetails() {
  const router = useRouter();
  return (
    <>
      <Head>
        <title>Details {router.query.id}</title>
        <meta name="description" content="Created by R444" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <HomeComponent.Layout>
        <HomeComponent.InplayDetails />
      </HomeComponent.Layout>
    </>
  );
}
