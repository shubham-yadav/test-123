import Head from "next/head";
import { HomeComponent } from "@/component";
import useSWR from "swr";

export default function Home(props) {
  const { sportsId } = props;
  useSWR("activeSports", () => {
    return sportsId;
  });
  return (
    <>
      <Head>
        <title>Inplay</title>
        <meta name="description" content="Created by R444" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <HomeComponent.Layout>
        <div className="maincol ">
          <div>
            <div className=" ">
              <div>
                <HomeComponent.InplayTable />
                <HomeComponent.Footer />
              </div>
            </div>
          </div>
        </div>
      </HomeComponent.Layout>
    </>
  );
}

// export async function getStaticProps() {
//   const res = await fetchData("getAllSports");
//   return {
//     props: {
//       data: res?.data,
//     },
//   };
// }

// export async function getStaticPaths() {
//   return {
//     paths: ["/sports"],
//     fallback: true,
//   };
// }
