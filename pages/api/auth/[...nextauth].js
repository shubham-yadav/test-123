import NextAuth from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
export const authOptions = {
  secret: "sk-test-1213",
  session: {
    strategy: "jwt",
  },
  providers: [
    CredentialsProvider({
      name: "Credentials",
      async authorize(credentials) {
        return credentials;
      },
    }),
  ],
  callbacks: {
    async jwt(props) {
      const { token, user, trigger } = props;
      if (trigger === "update") {
        return { isAuthenticate: false };
      } else {
        return { ...token, ...user };
      }
    },
    async session({ session, token }) {
      session.user = token;
      return session;
    },
  },
  pages: {
    signIn: "/sports",
  },
};
export default NextAuth(authOptions);
