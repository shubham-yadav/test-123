import React, { useEffect } from "react";
import Home from "./index";
import useSWR, { mutate } from "swr";
import { fetchData } from "@/services/swr";

export default function SportsDetails(props) {
  const { data } = useSWR("activeSports", () => {
    return props?.id;
  });

  useEffect(() => {
    mutate("activeSports", () => {
      return props?.id;
    });
  }, [props?.id]);
  return <Home sportsId={data} />;
}

export const getStaticProps = async (props) => {
  return { props: { id: props?.params.id} };
};
export async function getStaticParams() {
  return ["id", "data"];
}

export const getStaticPaths = async () => {
  const res = await fetchData("getAllSports");
  const paths = res?.data?.map((item) => ({ params: { id: item.sport_id.toString()} }) );
  return {
    paths,
    fallback: true,
  };
};
