import React from "react";
import Head from "next/head";
import { HomeComponent } from "@/component";

export default function Dashboard() {
  return (
    <section>
      <Head>
        <title>Home pages</title>
        <meta name="description" content="Created by R444" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <HomeComponent.Layout>
        <section className="charBx_sec">
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-6">
                <div className="card-accent-primary">
                  <div className="account-detail-head">Live Sports Profit</div>
                  <div className="account-stat-body">
                    <div className="highcharts-container">
                      <div className="grfCircle"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className="card-accent-primary">
                  <div className="account-detail-head">
                    {" "}
                    Backup Sports Profit
                  </div>
                  <div className="account-stat-body">
                    <div className="highcharts-container">
                      <div className="grfCircle"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </HomeComponent.Layout>
    </section>
  );
}
