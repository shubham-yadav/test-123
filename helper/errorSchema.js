import { constant } from "./constant";
import { utils } from "./utils";

// // const noNumber = /^([A-Za-z\u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff\s]*)$/gi;

// // export const loginSchema = Yup.object().shape({
// //   email: Yup.string()
// //     .email("must be valid email")
// //     .required("Please enter email"),
// //   password: Yup.string().required("Please enter your password"),
// // });

class Validator {
  constructor(value) {
    this.value = value;
    this.error = null;
  }

  required(errorMessage) {
    if (!this.value) {
      this.error = errorMessage ? errorMessage : constant.errorMessage.require;
    }

    return this;
  }

  string(errorMessage) {
    if (typeof this.value !== "string" || this.value.trim() === "") {
      this.error = errorMessage ? errorMessage : constant.errorMessage.string;
    }

    return this;
  }

  validate() {
    if (this.error) {
      throw new Error(this.error);
    } else {
      this.error = null;
      throw new Error(this.error);
    }
  }
}

const errorSchema = {
  loginSchema: {
    schem: {
      username: (v) => new Validator(v).required("Please enter your password"),
      password: (v) => new Validator(v).required(),
    },
    validate: function (obj) {
      const error = {};
      for (const key in obj) {
        try {
          error[key] = this.schem[key](obj[key]).validate();
        } catch (err) {
          if (err.message !== "null") {
            error[key] = err.message;
          }
        }
      }
      return utils.isObjectKeyEmpty(error) ? true : error;
    },
  },
};

export default errorSchema;
