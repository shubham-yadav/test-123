import { createContext } from "react";

const ContextHook = {
  LoginContext: createContext(false),
  LoginDetails: createContext({}), // {userDetails, fetchStatus}
};
export default ContextHook;
