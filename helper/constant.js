export const configRoute = {
  private: ["/my-profile", "/bets-history", "/profit-loss"],
};
export const constant = {
  sportsList: [
    { class: "Cricket", name: "Cricket", sport_id: 4 },
    { class: "Tennis", name: "Tennis", sport_id: 2 },
    { class: "Soccer", name: "Football", sport_id: 1 },
    { class: "Casino", name: "Casino", sport_id: 6 },
    { class: "Casino", name: "Matka", sport_id: 999 },
    {
      class: "Horse1 Racing",
      name: "Horse1 Racing",

      sport_id: 7,
    },
    { class: "Virtual Sports", name: "Virtual Sports", link: "/sports/" },
    {
      class: "Basketball",
      name: "Basketball",

      sport_id: 7522,
    },
  ],
  settingMenuList: [
    { name: "My Profile", link: "/my-profile" },
    { name: "Rolling Commission", link: "/my-profile" },
    { name: "Account Statement", link: "/my-profile" },
    { name: "Bets History", link: "/bets-history" },
    { name: "Profit & Loss", link: "/profit-loss" },
    { name: "Password History", link: "/my-profile" },
    { name: "Activity Log", link: "/my-profile" },
  ],
  menuList: [
    { name: "Home", link: "/sports", icon: "home.svg" },
    { name: "In-Play", link: "/inplay", icon: "timer.svg" },
    { name: "Sports", link: "/sports", icon: "trophy.svg" },
    {
      name: "Multi Markets",
      link: "/multi-markets",
      icon: "paper-push-pin.svg",
    },
    { name: "Account", link: "/my-profile", icon: "user.svg" },
  ],
  playMode: [
    {
      mode: 1,
      type: "Inplay",
    },
    {
      mode: 2,
      type: "Today",
    },
    { mode: 3, type: "Tommorrow" },
  ],
  errorMessage: {
    require: "field is required",
    string: "value must be string",
  },
};
