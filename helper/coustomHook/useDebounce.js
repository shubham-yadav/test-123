import { useEffect, useState } from "react";

export default function useDebounce(props) {
  const { value, delay } = props;
  const [debouncedValue, setDebouncedValue] = useState(value);

  useEffect(() => {
    const timeOut = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);

    return () => {
      clearTimeout(timeOut);
    };
  }, [value, delay]);

  return debouncedValue;
}
