import { getSession } from "next-auth/react";

export const setStorage = (key, data) => {
  if (typeof window !== "undefined") {
    localStorage.setItem(key, JSON.stringify(data));
  }
};

export const getStorage = async () => {
  let session = await getSession();
  return session?.user?.data && JSON.parse(session?.user?.data)?.token
    ? session?.user?.data && JSON.parse(session?.user?.data)?.token
    : null;
};

export const removeStorage = (key) => {
  let value = localStorage.removeItem(key);
  return value ? JSON.parse(value) : null;
};

export const clearStorage = (callBack) => {
  localStorage.clear();
  window.location.href = "/login";
  if (callBack) {
    callBack();
  }
};
